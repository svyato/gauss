def type_odds(x):  # Определяет дробное или целое число
    if '.' in x or ',' in x:
        return float(x)
    else:
        return int(x)


def odds(string):
    work = string.split('x2')
    a = work[0]
    b, c = work[1].split('x')

    # Определить тип коэффициентов
    a, b, c = type_odds(a), type_odds(b), type_odds(c)
    return a, b, c


def main():
    pass


if __name__ == "__main__":
    main()
