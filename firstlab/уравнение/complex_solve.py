import cmath


def complex_solve(a, b, d):
    x1 = (-b - cmath.sqrt(d)) / (2 * a)
    x2 = (-b + cmath.sqrt(d)) / (2 * a)
    return [x1, x2]


def create_answer_for_complex_solve(equat, x, vieta_hard_solve_flag):
    try:
        if x[0].real != 0:
            x1 = str(x[0])[1:-1]
        else:
            x1 = str(x[0])
        if x[1].real != 0:
            x2 = str(x[1])[1:-1]
        else:
            x2 = str(x[1])
        answer = f"Уравнение {equat}=0\n" \
                 f"Имеет следующие комплексные корни:\n" \
                 f"x1 = {x1}\n" \
                 f"x2 = {x2}"
        if vieta_hard_solve_flag:
            answer = "По теореме виета в диапазоне [-100;100] целых корней не найдено.\n" + answer
        return answer
    except:
        print("Ошибка формирования ответа для комплексных чисел!")


def main():
    pass


if __name__ == "__main__":
    main()
