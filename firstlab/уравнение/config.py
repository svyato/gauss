debug = True


def okruglenie (x):
    try:
        if len(x) == 2:
            x[0] = int(x[0] * 100) / 100
            x[1] = int(x[1] * 100) / 100
        else:
            x[0] = int(x[0] * 100) / 100
        return x
    except:
        x = int(x * 100) / 100
        return x


def end_conf(line, vieta, discr, complex, vi_hard, dis_1, dis_2):
    conf_after_run = f"line_flag = {line}\n" \
                     f"vieta_flag = {vieta}\n" \
                     f"discr_flag = {discr}\n" \
                     f"complex_flag = {complex}\n" \
                     f"vieta_hard_solve_flag = {vi_hard}\n" \
                     f"dis_1 = {dis_1}\n"\
                     f"dis_2 = {dis_2}"
    with open("end_conf", "w") as file:
        file.write(conf_after_run)

def main():
    pass



if __name__ == "__main__":
    main()
