from firstlab.уравнение.config import okruglenie
from firstlab.уравнение.line_solve import create_answer_for_line


def vieta(b, c):
    try:
        x = []
        for i in range(-100, 100):
            for j in range(-100, 100):
                if i + j == (-b) and i * j == c:
                    x.append(i)
                    x.append(j)
                    return x
        return  None
    except:
        print("Ошибка в vieta()")


def create_answer_for_vieta(a, b, c, equat, x):
    return create_answer_for_line(a, b, c, equat, x)


def main():
    pass


if __name__ == "__main__":
    main()