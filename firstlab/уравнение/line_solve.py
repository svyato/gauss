from firstlab.уравнение.config import okruglenie


def line_equat(b, c):
    return (-c) / b


def create_answer_for_line(a, b, c, equat, x):
    try:
        c = okruglenie(c)
        x = okruglenie(x)
        if len(x) == 2:
            if x[0] == x[1]:
                myanswer = f"Корень уравнения {equat}=0\n" \
                           f"x={x[0]}\n" \
                           f"Проверка:\n" \
                           f"f(x)={equat}\n" \
                           f"f({x[0]})=({okruglenie(a * x[0] ** 2)}) + ({okruglenie(b * x[0])})" \
                           f" + ({okruglenie(c)}) = {okruglenie(a * x[0] ** 2 + b * x[0] + c)} "
                return myanswer
            else:
                myanswer = f"Корни уравнения {equat}=0\n" \
                           f"x1={x[0]}\n" \
                           f"x2={x[1]}\n" \
                           f"Проверка:\n" \
                           f"f(x)={equat}\n" \
                           f"f({x[0]})=({okruglenie(a * x[0] ** 2)}) + ({okruglenie(b * x[0])}) + ({okruglenie(c)}) = {okruglenie(a * x[0] ** 2 + b * x[0] + c)} \n" \
                           f"f({x[1]})=({okruglenie(a * x[1] ** 2)}) + ({okruglenie(b * x[1])}) + ({okruglenie(c)}) = {okruglenie(a * x[1] ** 2 + b * x[1] + c)}"
                return myanswer
        elif len(x) == 1:
            myanswer = f"Корень уравнения {equat}=0\n" \
                       f"x={x[0]}\n" \
                       f"Проверка:\n" \
                       f"f(x)={equat}\n" \
                       f"f({x[0]})=({okruglenie(a * x[0] ** 2)}) + ({okruglenie(b * x[0])}) + ({c}) = " \
                       f"{okruglenie(a * x[0] ** 2 + b * x[0] + c)}"
            return myanswer
    except:
        print("Ошибка формирования ответа!")


def main():
    pass


if __name__ == "__main__":
    main()

