from firstlab.уравнение.line_solve import create_answer_for_line


def discriminant(a, b, c):
    return b ** 2 - 4 * a * c


def solve_dis_nol(a, b):
    return [-b / (2 * a)]


def solve_dis_plus(a, b, d):
    return [(-b - d ** 0.5) / (2 * a), (-b + d ** 0.5) / (2 * a)]


def create_answer_for_discriminant(vieta_hard_solve_flag, a, b, c, equat, x):
    answer = create_answer_for_line(a, b, c, equat, x)
    if vieta_hard_solve_flag:
        answer = "По теореме виета в диапазоне [-100;100] целых корней не найдено.\n" + answer
    return answer


def main():
    pass


if __name__ == "__main__":
    main()
