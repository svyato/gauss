from firstlab.уравнение.coeffs import odds
from firstlab.уравнение.complex_solve import complex_solve, create_answer_for_complex_solve
from firstlab.уравнение.discriminant_solve import discriminant, solve_dis_nol, solve_dis_plus, create_answer_for_discriminant
from firstlab.уравнение.line_solve import line_equat, create_answer_for_line
from firstlab.уравнение.vieta_solve import vieta, create_answer_for_vieta
from firstlab.уравнение.work_with_file import from_file, to_file
from firstlab.уравнение.config import debug, end_conf
from cmath import sqrt


def main():
    try:
        line_flag = False
        vieta_flag = False
        discr_flag = False
        complex_flag = False
        vieta_hard_solve_flag = False
        discr_2_x_flag = False
        discr_1_x_flag = False

        equat = from_file()  # Считать уравнение из файла
        a, b, c = odds(equat)  # Получить коэффициенты a, b, c

        if a == 0:
            line_flag = True  # Если а равен 0, решить линейное уравнение, получить единственный корень
        elif a == 1:
            vieta_flag = True
        else:
            discr_flag = True

        if line_flag:
            x = [line_equat(b, c)]
            myanswer = create_answer_for_line(a, b, c, equat, x)

            if debug:
                print(x)
                print(myanswer)

            to_file(myanswer)

        elif vieta_flag:
            x = vieta(b, c)
            if x is not None:
                myanswer = create_answer_for_vieta(a, b, c, equat, x)  # функция формирования строки ответа
                to_file(myanswer)
                if debug:
                    print(x)
            else:
                vieta_hard_solve_flag = True
                discr_flag = True

        # Если не получилось найти корень в диапазоне подбора, или а отлично от 0 и 1
        # Необходимо найти дискриминант
        if discr_flag:
            discr = discriminant(a, b, c)

            # Если дискриминант равен 0, найти единственный корень
            if discr == 0:
                discr_1_x_flag = True
                x = solve_dis_nol(a, b)
                if debug:
                    print(x)

            # Если дискриминант больше 0, найти два корня уравнения
            elif discr > 0:
                discr_2_x_flag = True
                x = solve_dis_plus(a, b, discr)
                if debug:
                    print(x)

            # Если дискриминант отрицательный, найти комлексные корни
            else:
                discr_flag = False
                complex_flag = True

            if discr_flag:
                myanswer = create_answer_for_discriminant(vieta_hard_solve_flag, a, b, c, equat, x)
                to_file(myanswer)
                if debug:
                    print(myanswer)

            if complex_flag:
                x = complex_solve(a, b, discr)
                myanswer = create_answer_for_complex_solve(equat, x, vieta_hard_solve_flag)
                to_file(myanswer)
                if debug:
                    print(myanswer)

        if debug:
            end_conf(line_flag, vieta_flag, discr_flag, complex_flag,
                     vieta_hard_solve_flag, discr_1_x_flag, discr_2_x_flag)
            print("Конечная конфигурация логических перепенных записана.")
    except:
        print("Ошибка!")


if __name__ == "__main__":
    main()


