import numpy as np

debug_thr = False


def main():
    pass


if __name__ == "__main__":
    main()


def thr_change_stolb(mat, b, num_stolb):
    m = mat.copy()
    m[0][num_stolb] = b[0][0]
    m[1][num_stolb] = b[1][0]
    m[2][num_stolb] = b[2][0]
    return m


def thr_kramer_solve(m, b):
    det_m = np.linalg.det(m)
    if det_m == 0:
        return None
    det_x1 = np.linalg.det(thr_change_stolb(m, b, 0))
    det_x2 = np.linalg.det(thr_change_stolb(m, b, 1))
    det_x3 = np.linalg.det(thr_change_stolb(m, b, 2))

    x1 = round(det_x1 / det_m, 2)
    x2 = round(det_x2 / det_m, 2)
    x3 = round(det_x3 / det_m, 2)
    if debug_thr:
        print(f"Корни по методу Крамера\nx1={x1}\nx2={x2}\nx3={x3}\n\n")
    return x1, x2, x3


# ************************************
# ************************************
# ************************************
# ************************************
debug_fr = False


def fr_change_stolb(mat, b, num_stolb):
    m = mat.copy()

    m[0][num_stolb] = b[0][0]
    m[1][num_stolb] = b[1][0]
    m[2][num_stolb] = b[2][0]
    m[3][num_stolb] = b[3][0]

    return m


def fr_kramer_solve(m, b):
    det_m = np.linalg.det(m)
    if det_m == 0:
        return None
    det_x1 = np.linalg.det(fr_change_stolb(m, b, 0))
    det_x2 = np.linalg.det(fr_change_stolb(m, b, 1))
    det_x3 = np.linalg.det(fr_change_stolb(m, b, 2))
    det_x4 = np.linalg.det(fr_change_stolb(m, b, 3))

    x1 = round(det_x1 / det_m, 2)
    x2 = round(det_x2 / det_m, 2)
    x3 = round(det_x3 / det_m, 2)
    x4 = round(det_x4 / det_m, 2)

    if debug_fr:
        print(f"Корни по методу Крамера\nx1={x1}\nx2={x2}\nx3={x3}\nx4={x4}\n")
    return x1, x2, x3, x4


def kramer_solve(m, b, num):
    if num == 3:
        return thr_kramer_solve(m, b)
    elif num == 4:
        return fr_kramer_solve(m, b)
