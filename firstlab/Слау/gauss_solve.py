import numpy as np

debug_gauss = False
debug_thr_sort_first_column = False
debug_thr_ones_first_column = False
debug_thr_zeros_first_column = False
debug_thr_sort_second_column = False
debug_thr_ones_second_column = False
debug_thr_zeros_second_column = False
debug_xyz = False


def thr_sort_first_column(m):
    if abs(m[0][0]) <= abs(m[1][0]):
        m[[0, 1]] = m[[1, 0]]
    if abs(m[1][0]) <= abs(m[2][0]):
        m[[1, 2]] = m[[2, 1]]
    if abs(m[0][0]) <= abs(m[1][0]):
        m[[0, 1]] = m[[1, 0]]
    if debug_thr_sort_first_column:
        print(F"Сортировка по первому столбцу:\n{m}")
    return m


def thr_ones_first_column(m):
    for i in range(3):
        temp = m[i][0]
        if temp == 0:
            print(f"\nГаусс. Значение первого коэффициента в {i} строке уже равно 0\nВозможны ошибки\nПриоритетным будет ответ,"
                  f"полученный иным способом")
            continue
        for j in range(4):
            m[i][j] = round(m[i][j] / temp, 2)

    if debug_thr_ones_first_column:
        print(f"Единицы в первом столбце:\n{m}\n\n")
    return m


def thr_zeros_first_column(m):
    m[1][0] -= m[0][0]
    m[1][1] -= m[0][1]
    m[1][2] -= m[0][2]
    m[1][3] -= m[0][3]

    m[2][0] -= m[0][0]
    m[2][1] -= m[0][1]
    m[2][2] -= m[0][2]
    m[2][3] -= m[0][3]

    if debug_gauss:
        print(f"Зануление первого столбца:\n{m}\n\n")
    return m


def thr_sort_second_column(m):
    if abs(m[1][1]) <= abs(m[2][1]):
        m[[1, 2]] = m[[2, 1]]

    if debug_thr_sort_second_column:
        print(f"Сортировка по второму столбцу:\n{m}\n\n")
    return m


def thr_ones_second_column(m):
    for i in range(1, 3):
        temp = m[i][1]
        if temp == 0:
            print(f"\nГаусс. Значение второго коэффициента в {i} строке уже равно 0\nВозможны ошибки\nПриоритетным будет ответ,"
                  f"полученный иным способом")
            if i == 1:
                pass  #
            continue
        for j in range(1, 4):
            m[i][j] = round(m[i][j] / temp, 2)
    if debug_thr_ones_second_column:
        print(f"Единицы во втором столбце:\n{m}\n\n")
    return m


def thr_zeros_second_column(m):
    m[2][1] -= m[1][1]
    m[2][2] -= m[1][2]
    m[2][3] -= m[1][3]
    if debug_thr_ones_second_column:
        print(f"Зануление второго столбца:\n{m}\n\n")
    return m


def xyz_solve(m):
    z = round(m[2][3] / m[2][2], 2)
    y = round((m[1][3] - z * m[1][2]) / m[1][1], 2)
    x = round((m[0][3] - z * m[0][2] - y * m[0][1]) / m[0][0], 2)
    if debug_xyz:
        print(f"x={x}\ny={y}\nz={z}\n")
    return x, y, z


def three_num_gauss(m):
    if debug_gauss:
        print("Функция three_num_gauss\n\n")

    m = thr_sort_first_column(m)
    m = thr_ones_first_column(m)
    m = thr_zeros_first_column(m)
    m = thr_sort_second_column(m)
    m = thr_ones_second_column(m)
    m = thr_zeros_second_column(m)
    return xyz_solve(m)


# *******************************************************************************************************
# *******************************************************************************************************
# *******************************************************************************************************
debug_fr_sort_first_column = False
debug_fr_ones_first_column = False
debug_fr_zeros_first_column = False
debug_fr_sort_second_column = False
debug_fr_ones_second_column = False
debug_fr_zeros_second_column = False
debug_fr_sort_third_column = False
debug_fr_ones_third_column = False
debug_fr_zeros_third_column = False


def fr_sort_first_column(m):
    mal = m[0][0]
    mnog = m[0][0]
    for i in range(4):
        if m[i][0] >= mnog:
            mnog = m[i][0]
            indmax = i
    m[[0, indmax]] = m[[indmax, 0]]

    for i in range(4):
        if m[i][0] <= mal:
            mal = m[i][0]
            indmin = i
    m[[3, indmin]] = m[[indmin, 3]]
    if debug_fr_sort_first_column:
        print(f"Частичная сортировка по первому столбцу:\n{m}")

    if abs(m[1][0]) <= abs(m[2][0]):
        m[[1, 2]] = m[[2, 1]]
    if debug_fr_sort_first_column:
        print(f"Полная сортировка по первому столбцу:\n{m}")
    return m


def fr_ones_first_column(m):
    for i in range(4):
        temp = m[i][0]
        if temp == 0:
            print(f"\nГаусс. Значение первого коэффициента в {i} строке уже равно 0\nВозможны ошибки\nПриоритетным будет ответ,"
                  f"полученный иным способом")
            continue
        for j in range(5):
            m[i][j] = round(m[i][j] / temp, 2)

    if debug_fr_ones_first_column:
        print(f"Единицы в первом столбце:\n{m}")

    return m


def fr_zeros_first_column(m):
    for i in range(1, 4):
        for j in range(5):
            m[i][j] -= m[0][j]
    if debug_fr_zeros_first_column:
        print(f"Обнуление первого столбца:\n{m}\n\n")
    return m


def fr_sort_second_column(m):
    if abs(m[1][1]) <= abs(m[2][1]):
        m[[1, 2]] = m[[2, 1]]
    if abs(m[2][1]) <= abs(m[3][1]):
        m[[2, 3]] = m[[3, 2]]
    if abs(m[1][1]) <= abs(m[2][1]):
        m[[1, 2]] = m[[2, 1]]

    if debug_fr_sort_second_column:
        print(f"Сортировка по второму столбцу:\n{m}\n\n")
    return m


def fr_ones_second_column(m):
    for i in range(1, 4):
        temp = m[i][1]
        if temp == 0:
            print(f"\nГаусс. Значение первого коэффициента в {i} строке уже равно 0\nВозможны ошибки\nПриоритетным будет ответ,"
                  f"полученный иным способом")
            continue
        for j in range(1, 5):
            m[i][j] = round(m[i][j] / temp, 2)

    if debug_fr_ones_second_column:
        print(f"Единицы во втором столбце:\n{m}")
    return m


def fr_zeros_second_column(m):
    m[2][1] -= m[1][1]
    m[2][2] -= m[1][2]
    m[2][3] -= m[1][3]
    m[2][4] -= m[1][4]

    m[3][1] -= m[1][1]
    m[3][2] -= m[1][2]
    m[3][3] -= m[1][3]
    m[3][4] -= m[1][4]

    if debug_fr_zeros_second_column:
        print(f"Зануления второго столбца:\n{m}")
    return m


def fr_sort_third_column(m):
    if abs(m[2][2]) <= abs(m[3][2]):
        m[[2, 3]] = m[[3, 2]]
    if debug_fr_sort_third_column:
        print(f"Отсортированный третий столбец:\n{m}\n\n")
    return m


def fr_ones_third_column(m):
    for i in range(2, 4):
        temp = m[i][2]
        if temp == 0:
            print(f"\nГаусс. Значение первого коэффициента в {i} строке уже равно 0\nВозможны ошибки\nПриоритетным будет ответ,"
                  f"полученный иным способом")
            continue
        for j in range(2, 5):
            m[i][j] = round(m[i][j] / temp, 2)
    if debug_fr_ones_third_column:
        print(f"Единицы в третьем столбце:\n{m}\n\n")
    return m


def fr_zeros_third_column(m):
    m[3][2] -= m[2][2]
    m[3][3] -= m[2][3]
    m[3][4] -= m[2][4]

    if debug_fr_zeros_third_column:
        print(f"Зануление третьего столбца:\n{m}")
    return m


def xyzc_solve(m):
    c = round(m[3][4] / m[3][3], 2)
    z = round((m[2][4] - m[2][3] * c) / m[2][2], 2)
    y = round((m[1][4] - m[1][3] * c - m[1][2] * z) / m[1][1], 2)
    x = round((m[0][4] - m[0][3] * c - m[0][2] * z - m[0][1] * y) / m[0][0], 2)
    if debug_gauss:
        print(f"Корни по методу Гаусса\nx={x}\ny={y}\nz={z}\nc={c}\n")
    return x, y, z, c


def four_num_gauss(m):
    m = fr_sort_first_column(m)
    m = fr_ones_first_column(m)
    m = fr_zeros_first_column(m)
    m = fr_sort_second_column(m)
    m = fr_ones_second_column(m)
    m = fr_zeros_second_column(m)
    m = fr_sort_third_column(m)
    m = fr_ones_third_column(m)
    m = fr_zeros_third_column(m)
    return xyzc_solve(m)


def gauss_solve(m, b, num):
    m = np.column_stack((m, b[:, 0]))
    if debug_gauss:
        print(m)

    if num == 3:
        return three_num_gauss(m)

    elif num == 4:
        return four_num_gauss(m)


def main():
    pass


if __name__ == "__main__":
    main()
