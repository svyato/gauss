import numpy as np
from firstlab.Слау.config import debug
from firstlab.Слау.gauss_solve import gauss_solve
from firstlab.Слау.kramer_solve import kramer_solve
from firstlab.Слау.matrix_solve import matrix_solve

# ****** Flags ****** #
four_lines_flag = False
three_lines_flag = False
error_lines_flag = False
error_x4_in_3_lines = False
fail_Gauss_flag = False
fail_Kramer_flag = False
fail_matrix_solve_flag = False

# ****** Flags ****** #
with open("data.txt") as file:
    SLAU = file.read()


def answer_to_file(gauss="", kramer="", matrix=""):
    ots = "\n\n"
    if fail_Gauss_flag:
        gauss = "Ошибка решения методом Гаусса\n"
    if fail_Kramer_flag:
        kramer = "Ошибка решения методом Крамера\n"
    if fail_matrix_solve_flag:
        matrix = "Ошибка решения матричным методом\n"
    answer = gauss + ots + kramer + ots + matrix
    with open("RESULT.txt", "w") as file:
        file.write(answer)
    return None


def create__answer(m, list_of_solves, b, method="gauss"):
    len_solves = len(list_of_solves)
    solve_met = ""
    if method == "gauss":
        solve_met = "Метод Гаусса"
    elif method == "kramer":
        solve_met = "Метод Крамера"
    elif method == "matrix":
        solve_met = "Матричный метод"
    if len_solves == 3:
        x1 = list_of_solves[0]
        x2 = list_of_solves[1]
        x3 = list_of_solves[2]

        answer = f"{solve_met}\n" \
                 f"Корни СЛАУ:\n" \
                 f"{SLAU}\n" \
                 f"x1 = {x1}\n" \
                 f"x2 = {x2}\n" \
                 f"x3 = {x3}\n" \
                 f"Проверка:\n" \
                 f" {m[0][0]} * {x1} + ({m[0][1]} * {x2}) + ({m[0][2]} * {x3}) = {b[0][0]}\n " \
                 f"{m[1][0]} * {x1} + ({m[1][1]} * {x2}) + ({m[1][2]} * {x3}) = {b[1][0]}\n " \
                 f"{m[2][0]} * {x1} + ({m[2][1]} * {x2}) + ({m[2][2]} * {x3}) = {b[2][0]}\n "
        if debug:
            print(answer)

    elif len_solves == 4:
        x1 = list_of_solves[0]
        x2 = list_of_solves[1]
        x3 = list_of_solves[2]
        x4 = list_of_solves[3]

        answer = f"{solve_met}\n" \
                 f"Корни СЛАУ:\n" \
                 f"{SLAU}\n" \
                 f"x1 = {x1}\n" \
                 f"x2 = {x2}\n" \
                 f"x3 = {x3}\n" \
                 f"x4 = {x4}\n" \
                 f"Проверка:\n" \
                 f" {m[0][0]} * {x1} + ({m[0][1]} * {x2}) + ({m[0][2]} * {x3}) + ({m[0][3]} * {x4}) = {b[0][0]}\n " \
                 f"{m[1][0]} * {x1} + ({m[1][1]} * {x2}) + ({m[1][2]} * {x3}) + ({m[1][3]} * {x4}) = {b[1][0]}\n " \
                 f"{m[2][0]} * {x1} + ({m[2][1]} * {x2}) + ({m[2][2]} * {x3}) + ({m[2][3]} * {x4}) = {b[2][0]}\n " \
                 f"{m[3][0]} * {x1} + ({m[3][1]} * {x2}) + ({m[3][2]} * {x3}) + ({m[3][3]} * {x4}) = {b[3][0]}\n "
        if debug:
            print(answer)



    else:
        global fail_Gauss_flag
        fail_Gauss_flag = True
        answer = "Ошибка решения методом Гаусса.\n"

    return answer


def get_matrix_coefs(slau):
    stolb_b = []
    stolb_x1 = []
    stolb_x2 = []
    stolb_x3 = []
    stolb_x4 = []

    for i in slau:
        try:
            if three_lines_flag and "x4" in i:
                global error_x4_in_3_lines
                error_x4_in_3_lines = True
                return None

            tmp = i.split("=")
            stolb_b.append(tmp[1])
            tmp = tmp[0].split("x1")
            stolb_x1.append(tmp[0])
            tmp = tmp[1].split("x2")
            stolb_x2.append(tmp[0])
            tmp = tmp[1].split("x3")
            stolb_x3.append(tmp[0])
            if four_lines_flag:
                tmp = tmp[1].split("x4")
                stolb_x4.append(tmp[0])
        except:
            print("Ошибка получения коэффициентов")

    if debug:
        print("Значения при каждой из неизвестных ")
        print(stolb_x1)
        print(stolb_x2)
        print(stolb_x3)
        print(stolb_x4)
        print(stolb_b)
        print("\n\n")

    try:
        if three_lines_flag:
            matrix = np.zeros((3, 3))
            svobod = np.zeros((3, 1))
        elif four_lines_flag:
            matrix = np.zeros((4, 4))
            svobod = np.zeros((4, 1))
    except:

        print("Ошибка создания ноль-матриц")

    num_lines = len(slau)
    for i in range(num_lines):
        matrix[i][0] = float(stolb_x1[i])
        matrix[i][1] = float(stolb_x2[i])
        matrix[i][2] = float(stolb_x3[i])
        svobod[i] = stolb_b[i]
        if four_lines_flag:
            matrix[i][3] = float(stolb_x4[i])

    if debug:
        print(f"Матрица после внесения в нее коэффициентов:\n{matrix}\n\n")
        print(f"Столбец свободных членов B:\n{svobod}\n\n")

    return matrix, svobod


def get_lines_flag(number):
    global four_lines_flag
    global three_lines_flag
    global error_lines_flag

    if number == 3:
        three_lines_flag = True
    elif number == 4:
        four_lines_flag = True
    else:
        error_lines_flag = True
    return None


def get_content():
    with open("data.txt") as file:
        content = file.read().split('\n')
        content = [i for i in content if len(i) >= 10]  # 10 знаков минимум в слау 3 порядка
    return content


def main():
    try:
        global fail_matrix_solve_flag
        global fail_Kramer_flag
        global fail_Gauss_flag
        slau_text = get_content()  # пункты 1, 2
        number_of_lines = len(slau_text)
        if debug:
            print(f"\nСЛАУ после раздееления по строкам:\n{slau_text}\n")
            print(f"Число строк в СЛАУ: {number_of_lines}\n\n")

        get_lines_flag(number_of_lines)
        if error_lines_flag:
            print("Ошибка входных данных")
            fail_Kramer_flag = True
            fail_Gauss_flag = True
            fail_matrix_solve_flag = True
            answer_to_file()
            return None

        matrix, stolb_b = get_matrix_coefs(slau_text)
        if error_x4_in_3_lines:
            print("Ошибка!\nх4 в СЛАУ из 3-х уравнений!\nМатрица не квадратная.")
            return None

        if debug:
            print(f"Тип переменной matrix: {type(matrix)}\n"
                  f"Тип переменной stolb_b: {type(stolb_b)}\n\n")

        # **********       **********
        # ********** Гаусс **********
        # **********       **********
        try:
            list_solves = list(gauss_solve(matrix, stolb_b, number_of_lines))
            end_gauss_answer = create__answer(matrix, list_solves, stolb_b, "gauss")
        except:
            fail_Gauss_flag = True
            print("Ошибка в методе Гаусса.")

        # **********       **********
        # **********       **********

        # **********        **********
        # ********** Крамер **********
        # **********        **********
        try:
            list_solves = list(kramer_solve(matrix, stolb_b, number_of_lines))
            end_kramer_answer = create__answer(matrix, list_solves, stolb_b, "kramer")

        except:
            fail_Gauss_flag = True
            print("Ошибка в методе Крамера")
        # **********        **********
        # **********        **********

        # **********           **********
        # ********** Матричный **********
        # **********           **********
        try:
            list_solves = matrix_solve(matrix, stolb_b)
            end_matrix_answer = create__answer(matrix, list_solves, stolb_b, "matrix")
        except:
            fail_matrix_solve_flag = True
            print("Ошибка в матричном методе")

        # **********           **********
        # **********           **********
        # **********           **********

        answer_to_file(end_gauss_answer, end_kramer_answer, end_matrix_answer)
        print("Завершено.")
    except:
        fail_Gauss_flag = True
        fail_matrix_solve_flag = True
        fail_Kramer_flag = True
        answer_to_file()


if __name__ == "__main__":
    main()
