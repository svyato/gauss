import numpy as np


def main():
    pass


if __name__ == "__main__":
    main()


def matrix_solve(m, b):
    det_m = np.linalg.det(m)
    if det_m == 0:
        return None
    mis = np.linalg.inv(m)
    solve = mis.dot(b)
    list_of_solves = [round(i[0], 2) for i in solve]
    return list_of_solves
